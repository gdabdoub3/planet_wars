f = open("win_log.txt", "r")
bots = ['Rage','Random','Bully','Dual','Expand','Prospector']
totalGames = 0
wins = 0
losses = 0
draws = 0
gamesLost = []
for line in f:
    if "Player 1 Wins!" in line:
        totalGames = totalGames+1
        wins = wins+1
    if "Player 2 Wins!" in line:
        gamesLost.append(totalGames)
        totalGames = totalGames+1
        losses = losses+1
        
    if "raw" in line:
        totalGames = totalGames+1
        draws = draws+1

print "Total Games: " + str(totalGames)
print "Wins: " + str(wins)+'/'+str(totalGames)
print "Losses: " + str(losses)+'/'+str(totalGames)
print "Draws" + str(draws)+'/'+str(totalGames)
print "Games Lost: "
for game in gamesLost:
    print bots[(game+1)/100] + ' ' + str((game%101) +1)
