from os import listdir
from os.path import isfile, join

mypath = "shiva_bots"

onlyfiles = [ f for f in listdir(mypath) if isfile(join(mypath,f)) ]

for n in onlyfiles:
    n = n.rstrip(".java")
    f = open(n+".cmd", 'w')
    f.write('java -jar tools/PlayGame.jar maps/map7.txt 1000 200 log.txt "java MyBot" "java shiva_bots/'+n+'" | java -jar tools/ShowGame.jar')
    f.close()

print onlyfiles






