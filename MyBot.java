import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;

@SuppressWarnings("unused")
public class MyBot {
	// ---------- debugging code start ------------ \\
	private static PrintWriter log;
	// ---------- debugging code end ------------ \\

	// ---------- Variables ---------------- \\
	private static final double THRESH = .5;
	private static final double ATTACK_FORCE = .75;
	private static final boolean DEBUG = false;

	// ---------- Variables end---------------- \\

	// ---------- Helper classes ---------- \\

	private static class Task {
		public enum Action {
			DEFEND, ATTACK, EXPAND, SUPPLY
		};

		public static final int DEFEND_PRIORITY = 4;
		public static final int SUPPLY_PRIORITY = 3;
		public static final int ATTACK_PRIORITY = 2;
		public static final int EXPAND_PRIORITY = 1;

		private PlanetWrapper sourcePlanet, destinationPlanet;
		private int basePriority, numShips;
		private double priorityMod;
		private Action action;

		public int getNumShips() {
			return numShips;
		}

		public void setNumShips(int numShips) {
			this.numShips = numShips;
		}

		public String toString() {
			return this.action.toString() + " " + Double.toString(this.getPriority() + this.getPriorityMod());
		}

		public PlanetWrapper getSourcePlanet() {
			return sourcePlanet;
		}

		public void setSourcePlanet(PlanetWrapper sourcePlanet) {
			this.sourcePlanet = sourcePlanet;
		}

		public static class TaskComparator implements Comparator<Task> {
			public int compare(Task taskA, Task taskB) {
				return (int) (taskB.getPriority() + taskB.getPriorityMod() - (taskA.getPriority() + taskA.getPriorityMod()) + THRESH);
			}
		}

		public void assignTask(PlanetWrapper p) {
			p.assignTask(this);
		}

		public int getPriority() {
			return basePriority;
		}

		public void setPriority(int priority) {
			this.basePriority = priority;
		}

		public double getPriorityMod() {
			return priorityMod;
		}

		public void setPriorityMod(double priorityMod) {
			this.priorityMod = priorityMod;
		}

		public PlanetWrapper getDestinationPlanet() {
			return destinationPlanet;
		}

		public void setDestinationPlanet(PlanetWrapper destinationPlanet) {
			this.destinationPlanet = destinationPlanet;
		}

		public Action getAction() {
			return action;
		}

		public void setAction(Action action) {
			this.action = action;
		}

		public Task(int priority, double priorityMod, PlanetWrapper sourcePlanet, PlanetWrapper destinationPlanet, Action action) {
			this.basePriority = priority;
			this.priorityMod = priorityMod;
			this.sourcePlanet = sourcePlanet;
			this.destinationPlanet = destinationPlanet;
			this.action = action;
		}

	}

	private static class PlanetWrapper extends Planet {

		private int netShips;
		private boolean busy;
		private Task task;

		public PlanetWrapper(int planetID, int owner, int numShips, int growthRate, double x, double y) {
			super(planetID, owner, numShips, growthRate, x, y);
			netShips = numShips;
			busy = false;
			task = null;
		}

		public PlanetWrapper(Planet planet) {
			super(planet.PlanetID(), planet.Owner(), planet.NumShips(), planet.GrowthRate(), planet.X(), planet.Y());
			netShips = NumShips();
			busy = false;
			task = null;
		}

		public PlanetWrapper(Planet planet, int netShips) {
			super(planet.PlanetID(), planet.Owner(), planet.NumShips(), planet.GrowthRate(), planet.X(), planet.Y());
			this.netShips = netShips;
			busy = false;
			task = null;
		}

		public void assignTask(Task task) {
			this.task = task;
		}

		public int getNetShips() {
			return netShips;
		}

		public void setNetShips(int netShips) {
			this.netShips = netShips;
		}

		public boolean isBusy() {
			return busy;
		}

		public void setBusy(boolean busy) {
			this.busy = busy;
		}

		public boolean equals(Planet planet) {

			return this.PlanetID() == planet.PlanetID();
		}

	}

	public static int getAttackingShips(PlanetWars pw, Planet mine) {

		int turnsAccountedFor = 0;
		int ships = 0;
		for (Fleet enemy : pw.EnemyFleets()) {
			if (enemy.DestinationPlanet() == mine.PlanetID()) {
				// log(Integer.toString(enemy.NumShips()) +
				// " ships will arrive in " +
				// Integer.toString(enemy.TurnsRemaining()) +" turns.");
				ships += enemy.NumShips();
				if (enemy.NumShips() >= mine.GrowthRate() * (enemy.TurnsRemaining() - turnsAccountedFor)) {
					ships -= mine.GrowthRate() * (enemy.TurnsRemaining() - turnsAccountedFor);
					turnsAccountedFor += enemy.TurnsRemaining();
				} else {
					ships -= enemy.NumShips();
					turnsAccountedFor += (int) (enemy.NumShips() / mine.GrowthRate() + 1);
				}
			}
		}
		if (mine.PlanetID() == 18){
			log("SHIPS1");
			log(ships);
		}
		for (Fleet f : pw.MyFleets()) {
			if (f.DestinationPlanet() == mine.PlanetID()) {
				ships -= f.NumShips();
			}
		}
		if (mine.PlanetID() == 18){
			log("SHIPS1");
			log(ships);
		}
		// log(ships);
		return ships;
	}

	public static int totalGrowth(int playerID, PlanetWars pw) {
		int growth = 0;

		for (Planet p : pw.Planets()) {
			if (p.Owner() == playerID) {
				growth += p.GrowthRate();
			}
		}

		return growth;

	}

	public static int averageGrowth(PlanetWars pw) {
		int growth = 0;

		for (Planet p : pw.Planets()) {
			growth += p.GrowthRate();
			
		}
		growth = growth / pw.NumPlanets();
		return growth;

	}
	
	// ---------- Helper classes end ---------- \\

	// The DoTurn function is where your code goes. The PlanetWars object
	// contains the state of the game, including information about all planets
	// and fleets that currently exist. Inside this function, you issue orders
	// using the pw.IssueOrder() function. For example, to send 10 ships from
	// planet 3 to planet 8, you would say pw.IssueOrder(3, 8, 10).
	//
	// There is already a basic strategy in place here. You can use it as a
	// starting point, or you can throw it out entirely and replace it with
	// your own. Check out the tutorials and articles on the contest website at
	// http://www.ai-contest.com/resources.
	static int counter = 0;

	public static void DoTurn(PlanetWars pw) {
		ArrayList<PlanetWrapper> myPlanets = new ArrayList<PlanetWrapper>();
		ArrayList<Task> tasks = new ArrayList<Task>();
		try {
			log("Turn: " + Integer.toString(counter));
			counter++;
			long startTime = System.nanoTime();
			for (Planet p : pw.MyPlanets()) {
				myPlanets.add(new PlanetWrapper(p, p.NumShips() - getAttackingShips(pw, p)));
				// log(p.NumShips() - getAttackingShips(pw, p));
			}
			long endTime = System.nanoTime();

			log("Duration of for: " + Long.toString((endTime - startTime)));

			startTime = System.nanoTime();
			tasks = taskGenerator(pw, myPlanets);
			endTime = System.nanoTime();
			log("Duration of taskGenerator: " + Long.toString(endTime - startTime));
			startTime = System.nanoTime();
			log(tasks);
			carryoutTasks(tasks, pw, startTime);
			endTime = System.nanoTime();
			log("Duration of carryoutTasks: " + Long.toString(endTime - startTime));
		} catch (Exception e) {
			log("Exceptions happen!: " + e); // this will put all the details in
												// log file configured earlier
		}

	}

	private static void carryoutTasks(ArrayList<Task> tasks, PlanetWars pw, long startTime) {
		int counter = 0;
		log("NumberOfTasks: " + Integer.toString(tasks.size()));
		for (Task task : tasks) {
/*
			log("	TASK: " + Integer.toString(counter));
			log("	" + task.action.name());
			log("	Time: " + Long.toString(System.nanoTime() - startTime));
			counter++;
			log("	" + Boolean.toString(task.action == Task.Action.DEFEND));
			log("	" + Boolean.toString(!task.sourcePlanet.busy && task.sourcePlanet.netShips > 0));
*/			
			if (task.action == Task.Action.DEFEND) {
				log("		DEFEND");
				task.sourcePlanet.setBusy(true);
			} else if (!task.sourcePlanet.busy && task.sourcePlanet.netShips > 0) {
				log("		In Else");
				if (task.action == Task.Action.SUPPLY) {
					log("			SUPPLY1");
					log("			PlanetS "+Integer.toString(task.getSourcePlanet().PlanetID()));
					log("			PlanetD "+Integer.toString(task.getDestinationPlanet().PlanetID()));
					log("			NetS "+Integer.toString(task.sourcePlanet.netShips));
					log("			NetD "+Integer.toString(task.destinationPlanet.netShips));
					log("			NumShips "+Integer.toString((task.getSourcePlanet().NumShips())));
					if (task.destinationPlanet.netShips < 0) {
						if (task.sourcePlanet.netShips > -1 * task.destinationPlanet.netShips + 1 && task.sourcePlanet.NumShips() >-1 * task.destinationPlanet.netShips + 1) {
							log("			Supply Size "+Integer.toString(-1 * task.destinationPlanet.netShips + 1));
							log("			DNET");
							pw.IssueOrder(task.sourcePlanet, task.destinationPlanet, -1 * task.destinationPlanet.netShips + 1);
							task.sourcePlanet.setNetShips(task.sourcePlanet.getNetShips() + task.destinationPlanet.netShips - 1);
							task.sourcePlanet.RemoveShips(-1 * task.destinationPlanet.netShips + 1);
							task.destinationPlanet.setNetShips(1);
						} else {
							if(task.getSourcePlanet().NumShips() <= task.getSourcePlanet().getNetShips()){
								log("			Supply Size "+task.sourcePlanet.NumShips());
								log("			NUM");
								pw.IssueOrder(task.sourcePlanet, task.destinationPlanet, task.sourcePlanet.NumShips());
								task.sourcePlanet.RemoveShips(task.sourcePlanet.NumShips());
								task.destinationPlanet.setNetShips(task.destinationPlanet.getNetShips() + task.sourcePlanet.getNetShips());
								task.sourcePlanet.setNetShips(0);
							} else {
								log("			Supply Size "+task.sourcePlanet.getNetShips());
								log("			NET");
								pw.IssueOrder(task.sourcePlanet, task.destinationPlanet, task.sourcePlanet.getNetShips());
								task.sourcePlanet.RemoveShips(task.sourcePlanet.getNetShips());
								task.destinationPlanet.setNetShips(task.destinationPlanet.getNetShips() + task.sourcePlanet.getNetShips());
								task.sourcePlanet.setNetShips(0);
							}
						}
					}

				} else if (task.action == Task.Action.ATTACK) {
					log("			ATTACK");
					log("			PlanetS "+Integer.toString(task.getSourcePlanet().PlanetID()));
					log("			PlanetD "+Integer.toString(task.getDestinationPlanet().PlanetID()));
					log("			Net "+Integer.toString(task.sourcePlanet.netShips));
					log("			NumShips "+Integer.toString((task.getSourcePlanet().NumShips())));
					
					
					if(task.getSourcePlanet().NumShips() <= task.getSourcePlanet().getNetShips()){
						log("			Attack Size "+Integer.toString((int) (task.sourcePlanet.NumShips() * ATTACK_FORCE)));
						log("			NUM");
						log("			Diff "+Integer.toString((task.getSourcePlanet().NumShips()) - (int) (task.sourcePlanet.NumShips() * ATTACK_FORCE)));
						pw.IssueOrder(task.sourcePlanet, task.destinationPlanet, (int) (task.sourcePlanet.NumShips() * ATTACK_FORCE));
						task.sourcePlanet.RemoveShips((int) (task.sourcePlanet.NumShips() * ATTACK_FORCE));
						task.sourcePlanet.setNetShips(task.sourcePlanet.netShips - (int) (task.sourcePlanet.NumShips() * ATTACK_FORCE));

					} else {
						log("			Attack Size "+Integer.toString((int) (task.sourcePlanet.netShips * ATTACK_FORCE)));
						log("			NET");
						log("			Diff "+Integer.toString((task.getSourcePlanet().NumShips()) - (int) (task.sourcePlanet.netShips * ATTACK_FORCE)));
						pw.IssueOrder(task.sourcePlanet, task.destinationPlanet, (int) (task.sourcePlanet.netShips * ATTACK_FORCE));
						task.sourcePlanet.setNetShips(task.sourcePlanet.netShips - (int) (task.sourcePlanet.netShips * ATTACK_FORCE));
						task.sourcePlanet.RemoveShips((int) (task.sourcePlanet.netShips * ATTACK_FORCE));

					}

				} else if (task.action == Task.Action.EXPAND && task.destinationPlanet.netShips - shipsGoingTo(pw, task.destinationPlanet) > 0) {
					log("			EXPAND");
					if (task.sourcePlanet.netShips > task.destinationPlanet.netShips + 1 && task.sourcePlanet.NumShips() > task.destinationPlanet.netShips + 1) {
						log("				EXPAND IF");
						pw.IssueOrder(task.sourcePlanet, task.destinationPlanet, task.destinationPlanet.NumShips() + 1);
						task.sourcePlanet.setNetShips(task.sourcePlanet.getNetShips() - task.destinationPlanet.netShips - 1);
						task.destinationPlanet.setNetShips(0);
						task.sourcePlanet.RemoveShips(task.destinationPlanet.NumShips() + 1);
					} else {
						log("				EXPAND ELSE");
						if(task.getSourcePlanet().NumShips() <= task.getSourcePlanet().getNetShips()){
							pw.IssueOrder(task.sourcePlanet, task.destinationPlanet, task.sourcePlanet.NumShips());
							task.destinationPlanet.setNetShips(task.destinationPlanet.getNetShips() - task.sourcePlanet.NumShips());
							task.sourcePlanet.setNetShips(0);
							task.sourcePlanet.RemoveShips(task.sourcePlanet.NumShips());
						} else {
							pw.IssueOrder(task.sourcePlanet, task.destinationPlanet, task.sourcePlanet.getNetShips());
							task.destinationPlanet.setNetShips(task.destinationPlanet.getNetShips() - task.sourcePlanet.getNetShips());
							task.sourcePlanet.setNetShips(0);
							task.sourcePlanet.RemoveShips( task.sourcePlanet.getNetShips());
						}

					}

				}

			}

			
		}

		
	}

	public static int shipsGoingTo(PlanetWars pw, Planet p) {

		int netShips = 0;

		for (Fleet me : pw.MyFleets()) {
			if (me.DestinationPlanet() == p.PlanetID()) {
				netShips += me.NumShips();
			}

		}
		for (Fleet enemy : pw.EnemyFleets()) {
			if (enemy.DestinationPlanet() == p.PlanetID()) {
				netShips -= enemy.NumShips();
			}

		}

		return netShips;
	}

	public static ArrayList<Task> taskGenerator(PlanetWars pw, ArrayList<PlanetWrapper> myPlanets) {
		ArrayList<Task> tasks = new ArrayList<Task>();

		for (Planet enemy : pw.EnemyPlanets()) {

			for (PlanetWrapper mine : myPlanets) {
				tasks.add(new Task(Task.ATTACK_PRIORITY, calcAttackMod(mine, enemy, pw), mine, new PlanetWrapper(enemy), Task.Action.ATTACK));
			}

		}

		for (PlanetWrapper target : myPlanets) {

			tasks.add(new Task(Task.DEFEND_PRIORITY, calcDefendMod(target), target, target, Task.Action.DEFEND));

			for (PlanetWrapper mine : myPlanets) {
				if (mine.PlanetID() != target.PlanetID()) {
					tasks.add(new Task(Task.SUPPLY_PRIORITY, calcSupplyMod(mine, target, pw), mine, target, Task.Action.SUPPLY));
				}
			}

		}

		for (Planet neutral : pw.NeutralPlanets()) {

			for (PlanetWrapper mine : myPlanets) {
				tasks.add(new Task(Task.EXPAND_PRIORITY, calcExpandMod(mine, neutral, pw), mine, new PlanetWrapper(neutral), Task.Action.EXPAND));
			}

		}

		Collections.sort(tasks, new Task.TaskComparator());
		log(tasks);
		return tasks;
	}
	
	public static int distanceToClosestEnemy(PlanetWars pw, Planet p){
		int minDist = 100;
		for(Planet enemy : pw.EnemyPlanets()){
			if(pw.Distance(p.PlanetID(), enemy.PlanetID())<minDist){
				minDist = pw.Distance(p.PlanetID(), enemy.PlanetID());
			}
		}
		return minDist;
	}

	// ------------- Modifier Calculators --------------- \\
	public static double calcAttackMod(PlanetWrapper mine, Planet enemy, PlanetWars pw) {
		double mod = 0;
		//double growth = totalGrowth(1, pw) / totalGrowth(2, pw);
		int myShips = pw.NumShips(1);
		int hisShips = enemy.NumShips()+(pw.Distance(mine.PlanetID(), enemy.PlanetID())*enemy.GrowthRate() );

		//mod += growth - 1;
		if (myShips > hisShips && mine.getNetShips() >= hisShips){
			mod += 3;
		} else {
			mod -= 3;
		}
		
		if (totalGrowth(1, pw) > (totalGrowth(2, pw))){
			mod +=1;
		}else {
			mod -=1;
		}
		
		
		//mod += myShips / hisShips - 1;
		//mod += (1 / pw.Distance(mine.PlanetID(), enemy.PlanetID()) - .75) * 3;
		
		if(pw.Distance(mine.PlanetID(), enemy.PlanetID()) == distanceToClosestEnemy(pw, mine)){
			mod += 1;
		}

		return mod;

	}

	public static double calcDefendMod(PlanetWrapper mine) {

		if (mine.netShips <= 0) {
			return 5;
		}

		return 0;

	}

	public static double calcExpandMod(PlanetWrapper mine, Planet neutral, PlanetWars pw) {
		double mod = 0;
	
		/*
		mod += neutral.GrowthRate() / 5;
		mod += mine.NumShips() / neutral.NumShips() - 1;
		mod += (1 / pw.Distance(mine.PlanetID(), neutral.PlanetID()) - .75) * 3;
	
		*/
	//	/*	
		int myShips = pw.NumShips(1);
		int hisShips = neutral.NumShips();
		
		if (neutral.GrowthRate() >= averageGrowth(pw)){
			mod += 1;
		}
		

		//mod += growth - 1;
		if (myShips > hisShips && mine.getNetShips() >= hisShips){
			mod += 2;
		} else {
			mod -= 3;
		}
		
		if (pw.Distance(mine.PlanetID(), neutral.PlanetID()) < distanceToClosestEnemy(pw, mine)){
			mod += 1;
			if (pw.Distance(mine.PlanetID(), neutral.PlanetID()) < distanceToClosestEnemy(pw, mine)/2){
				mod+=2;
			}
		} else {
			mod -= 3;
		}
		
//	*/	
		
		return mod;

	}
//TODO
	public static double calcSupplyMod(PlanetWrapper mine, PlanetWrapper target, PlanetWars pw) {

		int minDistFleet = 100;
		for (Fleet f : pw.EnemyFleets()) {
			if (f.DestinationPlanet() == target.PlanetID() && f.TurnsRemaining() < minDistFleet) {
				minDistFleet = f.TurnsRemaining();
			}
		}

		if (minDistFleet == 100) {
			minDistFleet = 0;
		}

		if (pw.Distance(mine.PlanetID(), target.PlanetID()) <= minDistFleet) {
			if (target.netShips < 0) {
				return 4;
			} else {
				return 1;
			}
		}

		return 0;

	}

	// ------------- Modifier Calculators End --------------- \\

	// ---------- Debugging code start ------------ \\
	public static void log(String s) {
		if (DEBUG) {
			log.println(s);
			log.flush();
		}
	}

	public static void log(int i) {
		if (DEBUG) {
			log.println(i);
			log.flush();
		}
	}

	public static void log(double d) {
		if (DEBUG) {
			log.println(d);
			log.flush();
		}
	}

	public static void log(Object o) {
		if (DEBUG) {
			log.println(o);
			log.flush();
		}
	}

	// ---------- Debugging code end ------------ \\

	public static void main(String[] args) {
		// ---------- Debugging code start ------------ \\
		try {
			log = new PrintWriter("output.txt", "UTF-8");
		} catch (FileNotFoundException e1) {

		} catch (UnsupportedEncodingException e1) {

		}
		// ---------- debugging code end ------------ \\
		String line = "";
		String message = "";
		int c;
		try {
			while ((c = System.in.read()) >= 0) {
				switch (c) {
				case '\n':
					if (line.equals("go")) {
						PlanetWars pw = new PlanetWars(message);
						DoTurn(pw);
						pw.FinishTurn();
						message = "";
					} else {
						message += line + "\n";
					}
					line = "";
					break;
				default:
					line += (char) c;
					break;

				}
			}
		} catch (Exception e) {
			// Owned.
		}
		// ---------- debugging code start ------------ \\
		log.close();
		// ---------- debugging code end ------------ \\
	}
}